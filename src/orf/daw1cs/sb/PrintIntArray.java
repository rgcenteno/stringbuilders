/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orf.daw1cs.sb;

/**
 *
 * @author rgcenteno
 */
public class PrintIntArray {
    
    public static void printIntArrayProgram(){
        int elementos = -1;
        java.util.Scanner tecladoLocal = StringBuildersProgram.teclado;
        do{
            System.out.println("Por favor inserte cuantos elementos quiere que tenga el array");
            if(tecladoLocal.hasNextInt()){
                elementos = tecladoLocal.nextInt();
                if(elementos <= 0){
                    System.out.println("Por favor inserte un número positivo mayor que cero");
                }
            }
            tecladoLocal.nextLine();
        }while(elementos <= 0);
        
        int[] array = new int[elementos];
        java.util.Random generador = new java.util.Random();
        for (int i = 0; i < array.length; i++) {            
            int aux = generador.nextInt(elementos * 10) + 1;
            array[i] = aux;
        }
        System.out.println(arrayToString(array));
    }
    
    private static String arrayToString(int[] array){
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < array.length; i++) {
            int j = array[i];
            sb.append(j).append(", ");
        }
        sb.replace(sb.length() - 2, sb.length(), "]");
        return sb.toString();
    }
}
