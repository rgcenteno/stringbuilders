/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orf.daw1cs.sb;

/**
 *
 * @author rgcenteno
 */
public class StringBuildersProgram {
    
    public static java.util.Scanner teclado = new java.util.Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String opcion = "";
        do{
            System.out.println("*******************************************");
            System.out.println("* 1. Solo letras y guiones                *");
            System.out.println("* 2. Print array                          *");
            System.out.println("* 3. Anagrama                             *");                        
            System.out.println("*                                         *");
            System.out.println("* 0. Salir                                *");
            System.out.println("*******************************************");
            opcion = teclado.nextLine();
        
            switch(opcion){
                case "1":
                    soloLetrasYGuionesProgram();
                    break;
                case "2":
                    PrintIntArray.printIntArrayProgram();
                    break;
                case "3":
                    Anagrama.anagramaProgram();
                    break;                
                case "0":
                    break;
                default:
                    System.out.println("Opción inválida");

            }
        }while(!opcion.equalsIgnoreCase("0"));
    }
    
    private static void soloLetrasYGuionesProgram(){
        System.out.println("Por favor inserte la cadena a transformar");
        String input = teclado.nextLine();
        System.out.printf("La cadena transformada es: \n%s\n\n", soloLetrasYGuiones(input));
        
    }
    
    private static String soloLetrasYGuiones(String input){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if(Character.isLetter(c)){
                sb.append(c);
            }
            else{
                sb.append('_');
            }
        }
        return sb.toString();
    }
    
}
