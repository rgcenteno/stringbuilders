/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orf.daw1cs.sb;

/**
 *
 * @author rgcenteno
 */
public class Anagrama {
    
    public static void anagramaProgram(){
        java.util.Scanner tecladoLocal = StringBuildersProgram.teclado;
        System.out.println("Por favor, inserte la primera palabra");
        String s1 = tecladoLocal.nextLine();
        System.out.println("Por favor, inserte la segunda palabra");
        String s2 = tecladoLocal.nextLine();
        System.out.println("Son anagrama? " + esAnagrama(s1, s2));        
    }
    
    private static boolean esAnagrama(String s1, String s2){
        StringBuilder sb2 = new StringBuilder(s2);
        for (int i = 0; i < s1.length(); i++) {
            char c = s1.charAt(i);            
            int index = sb2.indexOf(String.valueOf(c));
            if(index == -1){
                return false;
            }
            else{
                sb2.deleteCharAt(index);
            }
        }
        
        return sb2.length() == 0;
    }
}
